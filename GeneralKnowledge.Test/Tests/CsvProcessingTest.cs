﻿using Microsoft.VisualBasic.FileIO;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// CSV processing test
    /// </summary>
    public class CsvProcessingTest : ITest
    {
        public class Asset
        {
            public string assetid { get; set; }
            public string file_name { get; set; }
            public string mime_type { get; set; }
            public string created_by { get; set; }
            public string email { get; set; }
            public string country { get; set; }
            public string description { get; set; }
        }
        public void Run()
        {
            // TODO
            // Create a domain model via POCO classes to store the data available in the CSV file below
            // Objects to be present in the domain model: Asset, Country and Mime type
            // Process the file in the most robust way possible
            // The use of 3rd party plugins is permitted

            List<Asset> assets = new List<Asset>();
            var path = @"../../Resources/AssetImport.csv";
            using (TextFieldParser csvParser = new TextFieldParser(path))
            {
                csvParser.CommentTokens = new string[] { "#" };
                csvParser.SetDelimiters(new string[] { "," });
                csvParser.HasFieldsEnclosedInQuotes = true;

                // Skip the row with the column names
                csvParser.ReadLine();

                while (!csvParser.EndOfData)
                {
                    // Read current line fields, pointer moves to the next line.
                    string[] fields = csvParser.ReadFields();
                    assets.Add(new Asset
                    {
                        assetid = fields[0],
                        file_name = fields[1],
                        mime_type = fields[2],
                        created_by = fields[3],
                        email = fields[4],
                        country = fields[5],
                        description = fields[6]
                    });
                }
            }

            //var csvFile = Resources.AssetImport;
        }

        public static IEnumerable<Asset> ReturnAsset()
        {
            List<Asset> assets = new List<Asset>();
            var path = @"../../Resources/AssetImport.csv";
            using (TextFieldParser csvParser = new TextFieldParser(path))
            {
                csvParser.CommentTokens = new string[] { "#" };
                csvParser.SetDelimiters(new string[] { "," });
                csvParser.HasFieldsEnclosedInQuotes = true;

                // Skip the row with the column names
                csvParser.ReadLine();

                while (!csvParser.EndOfData)
                {
                    // Read current line fields, pointer moves to the next line.
                    string[] fields = csvParser.ReadFields();
                    assets.Add(new Asset
                    {
                        assetid = fields[0],
                        file_name = fields[1],
                        mime_type = fields[2],
                        created_by = fields[3],
                        email = fields[4],
                        country = fields[5],
                        description = fields[6]
                    });
                }
            }

            return assets;
        }
    }

}
