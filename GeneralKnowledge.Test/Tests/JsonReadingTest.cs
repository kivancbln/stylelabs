﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GeneralKnowledge.Test.App.Tests
{
    /// <summary>
    /// Basic data retrieval from JSON test
    /// </summary>
    public class JsonReadingTest : ITest
    {
        public string Name { get { return "JSON Reading Test";  } }
        public class samples
        {
            public DateTime date { get; set; }
            public double temperature { get; set; }
            public double pH { get; set; }
            public double phosphate { get; set; }
            public double chloride { get; set; }
            public double nitrate { get; set; }
        }
        public class JsonData
        {
            public List<samples> samples { get; set; }
        }

        public void Run()
        {
            var jsonData = Resources.SamplePoints;

            // TODO: 
            // Determine for each parameter stored in the variable below, the average value, lowest and highest number.
            // Example output
            // parameter   LOW AVG MAX
            // temperature   x   y   z
            // pH            x   y   z
            // Chloride      x   y   z
            // Phosphate     x   y   z
            // Nitrate       x   y   z
            PrintOverview(jsonData);
        }

        private void PrintOverview(byte[] data)
        {
            string jsonstr =System.Text.Encoding.Default.GetString(data);
            var classdata = Newtonsoft.Json.JsonConvert.DeserializeObject<JsonData>(jsonstr);

            Console.WriteLine("parameter\t\tLOW\tAVG\tMAX");
            Console.WriteLine("temperature\t\t" + Math.Round(classdata.samples.Min(x => x.temperature),2) + "\t" + Math.Round(classdata.samples.Average(x => x.temperature), 2) + "\t" + Math.Round(classdata.samples.Max(x => x.temperature), 2));
            Console.WriteLine("pH\t\t\t" + Math.Round(classdata.samples.Min(x => x.pH), 2) + "\t" + Math.Round(classdata.samples.Average(x => x.pH), 2) + "\t" + Math.Round(classdata.samples.Max(x => x.pH), 2));
            Console.WriteLine("chloride\t\t" + Math.Round(classdata.samples.Min(x => x.chloride), 2) + "\t" + Math.Round(classdata.samples.Average(x => x.chloride), 2) + "\t" + Math.Round(classdata.samples.Max(x => x.chloride), 2));
            Console.WriteLine("phosphate\t\t" + Math.Round(classdata.samples.Min(x => x.phosphate),2) + "\t" + Math.Round(classdata.samples.Average(x => x.phosphate), 2) + "\t" + Math.Round(classdata.samples.Max(x => x.phosphate), 2));
            Console.WriteLine("nitrate\t\t\t" + Math.Round(classdata.samples.Min(x => x.nitrate), 2) + "\t" + Math.Round(classdata.samples.Average(x => x.nitrate), 2) + "\t" + Math.Round(classdata.samples.Max(x => x.nitrate), 2));

        }
    }
}
