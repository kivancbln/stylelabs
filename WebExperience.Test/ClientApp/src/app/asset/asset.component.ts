import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-asset',
  templateUrl: './asset.component.html',
  styleUrls: ['./asset.component.css']
})
export class AssetComponent implements OnInit {
  assets: any;
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.http.get('/api/Asset').subscribe(data => {
      this.assets = data;
    });
  }

}
