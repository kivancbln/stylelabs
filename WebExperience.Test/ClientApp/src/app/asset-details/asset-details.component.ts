import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-asset-details',
  templateUrl: './asset-details.component.html',
  styleUrls: ['./asset-details.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AssetDetailsComponent implements OnInit {
  asset:any = {};
  constructor(private router: Router, private route: ActivatedRoute, private http: HttpClient) { }

  ngOnInit() {
    this.getAssetDetail(this.route.snapshot.params['id']);
  }

  getAssetDetail(id) {
    this.http.get('/api/Asset/' + id).subscribe(data => {
      this.asset = data;
    });
  }

  deleteAsset(id) {
    this.http.delete('/api/Asset/' + id)
      .subscribe(res => {
        this.router.navigate(['/Asset']);
      }, (err) => {
        console.log(err);
      }
      );
  }

}
