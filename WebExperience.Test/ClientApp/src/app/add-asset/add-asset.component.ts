import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http'

@Component({
  selector: 'app-add-asset',
  templateUrl: './add-asset.component.html',
  styleUrls: ['./add-asset.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AddAssetComponent implements OnInit {
  asset : any = {};
  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
  }

  createAsset() {
    this.http.post('/api/Asset', this.asset)
        .subscribe(res => {
          let id = res['Id'];
          this.router.navigate(['/details', id]);
        }, (err) => {
          console.log(err);
        }
      );
  }

}
