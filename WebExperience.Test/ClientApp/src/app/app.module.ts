import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { AssetDetailsComponent } from './asset-details/asset-details.component';
import { AssetComponent } from './asset/asset.component';
import { AddAssetComponent } from './add-asset/add-asset.component';
import { EditAssetComponent } from './edit-asset/edit-asset.component';

const appRoutes: Routes = [
  {
    path: 'details/:id',
    component: AssetDetailsComponent,
    data: { title: 'Asset Details' }
  },
  {
    path: '',
    component: AssetComponent,
    data: { title: 'Asset List' }
  },
  {
    path: 'assets',
    component: AssetComponent,
    data: { title: 'Asset List' }
  },
  {
    path: 'create',
    component: AddAssetComponent,
    data: { title: 'Add Asset' }
  },
  {
    path: 'edit/:id',
    component: EditAssetComponent,
    data: { title: 'Edit Asset' }
  }
];

@NgModule({
  declarations: [
    AppComponent,
    AssetComponent,
    AssetDetailsComponent,
    AddAssetComponent,
    EditAssetComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
