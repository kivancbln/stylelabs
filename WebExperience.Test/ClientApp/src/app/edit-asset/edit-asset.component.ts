import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-edit-asset',
  templateUrl: './edit-asset.component.html',
  styleUrls: ['./edit-asset.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class EditAssetComponent implements OnInit {
  asset : any = {};
  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getAssetDetail(this.route.snapshot.params['id']);
  }

  getAssetDetail(id) {
    this.http.get('/api/Asset/' + id).subscribe(data => {
      this.asset = data;
    });
  }
  updateAsset(id) {
    this.http.put('/api/Asset/' + id, this.asset)
      .subscribe(res => {
        let id = res['Id'];
        this.router.navigate(['/details', id]);
      }, (err) => {
        console.log(err);
      }
      );
  }
  deleteAsset(id) {
    this.http.delete('/api/Asset/' + id)
      .subscribe(res => {
        this.router.navigate(['/Asset']);
      }, (err) => {
        console.log(err);
      }
      );
  }
}
