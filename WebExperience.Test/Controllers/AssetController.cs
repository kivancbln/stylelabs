﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using WebExperience.Test.Models;

namespace WebExperience.Test.Controllers
{
    public class AssetController : ApiController
    {
        // TODO
        // Create an API controller via REST to perform all CRUD operations on the asset objects created as part of the CSV processing test
        // Visualize the assets in a paged overview showing the title and created on field
        // Clicking an asset should navigate the user to a detail page showing all properties
        // Any data repository is permitted
        // Use a client MVVM framework

        private AppContext db = new AppContext();

        // GET: api/Asset
        public IQueryable<AssetModel> GetAssets()
        {
            return db.Assets;
        }

        // GET: api/Asset/5
        [ResponseType(typeof(AssetModel))]
        public IHttpActionResult GetAsset(string id)
        {
            AssetModel assetModel = db.Assets.Find(id);
            if (assetModel == null)
            {
                return NotFound();
            }

            return Ok(assetModel);
        }

        // POST: api/Asset
        [ResponseType(typeof(AssetModel))]
        public IHttpActionResult PostAsset(AssetModel assetModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Assets.Add(assetModel);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { assetid = assetModel.assetid }, assetModel);
        }

        // PUT: api/Asset/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAsset(string id, AssetModel assetModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != assetModel.assetid)
            {
                return BadRequest();
            }

            db.Entry(assetModel).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AssetExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(assetModel);
        }

        // DELETE: api/Asset/5
        [ResponseType(typeof(AssetModel))]
        public IHttpActionResult DeleteAsset(string id)
        {
            AssetModel assetModel = db.Assets.Find(id);
            if (assetModel == null)
            {
                return NotFound();
            }

            db.Assets.Remove(assetModel);
            db.SaveChanges();

            return Ok(assetModel);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AssetExists(string id)
        {
            return db.Assets.Count(e => e.assetid == id) > 0;
        }

    }
}
