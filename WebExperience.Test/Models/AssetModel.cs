﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebExperience.Test.Models
{
    public class AssetModel
    {
        [Key]
        public string assetid { get; set; }
        [Required]
        public string file_name { get; set; }
        [Required]
        public string mime_type { get; set; }
        [Required]
        public string created_by { get; set; }
        [Required]
        public string email { get; set; }
        [Required]
        public string country { get; set; }
        [Required]
        public string description { get; set; }
    }
}